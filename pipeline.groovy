pipeline {
    agent any

    environment {
        APPSETTINGS_PATH = 'MedicalAppointmentSchedulingSystem/appsettings.json'
        DOCKER_COMPOSE_FILE = 'docker-compose.yml'
        DOCKER_COMPOSE_OVERRIDE_FILE = 'docker-compose.override.yml'
        DOCKERFILE = 'MedicalAppointmentSchedulingSystem/Dockerfile'
        PATH = "$PATH:/usr/local/bin"
        APPDATA = '/root/.local/share'
        SQLPACKAGE_PATH = 'C:\\Program Files\\Microsoft Visual Studio\\2022\\Preview\\Common7\\IDE\\Extensions\\Microsoft\\SQLDB\\DAC\\SqlPackage.exe'
        
    }

    stages {
        stage('Checkout') {
            steps {
                script {
                    echo 'Checking out source code...'
                    checkout scm
                    echo 'Source code checked out successfully.'
                }
            }
        }

        stage('List Files') {
            steps {
                script {
                    echo 'Listing files in workspace...'
                    bat 'dir'
                }
            }
        }

        stage('Build Docker Image') {
            steps {
                script {
                    echo 'Building Docker image...'
                    def image = docker.build("medical-appointment-scheduling", "-f ${DOCKERFILE} .")
                    echo 'Docker image built successfully.'
                }
            }
        }

        stage('Stop Existing Containers') {
            steps {
                script {
                    echo 'Stopping and removing existing containers...'
                    bat "docker-compose -f ${DOCKER_COMPOSE_FILE} -f ${DOCKER_COMPOSE_OVERRIDE_FILE} down"
                }
            }
        }

        stage('Start Services') {
            steps {
                script {
                    echo 'Starting services using Docker Compose...'
                    bat "docker-compose -f ${DOCKER_COMPOSE_FILE} -f ${DOCKER_COMPOSE_OVERRIDE_FILE} up -d"
                    echo 'Services started successfully.'
                }
            }
        }

        stage('Wait for Database Initialization') {
            steps {
                script {
                    echo 'Waiting for the database to be ready...'
                    sleep 60  // Adjust the time as needed for your DB to initialize
                }
            }
        }

        stage('Download Database Backup file & Restore Database') {
            steps {
                script {
                    echo 'Downloading database backup...'
                    bat 'copy devops_project\\MedicalAppointmentSchedulingDB.bacpac D:\\Devops\\Ass\\Backup\\MedicalAppointmentSchedulingDB.bacpac'
                    echo 'Database backup downloaded successfully.'
                }
            }
        }

      

        stage('Notify User') {
            steps {
                script {
                    echo 'Pipeline completed. Please open http://localhost:8001 in your web browser.'
                }
            }
        }

    }
}